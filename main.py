import praw
import requests
import re as regex
import getpass
import os
import sys
DIR_PATH = "./reddit_bookmark_pics/"
REGEX_IMAGE_EXT = "(.jpg)|(.jpeg)|(.png)"


def login():
    print("Username:")
    username = str(input("> "))
    print("Password:")
    password = str(getpass.getpass("> "))
    print("")

    return praw.Reddit(client_id='gDpMmHCGRPQRMg',
                       client_secret="Knphkg4vhs1ts5l8hhpUZaEKsjo",
                       password=password,
                       user_agent='reddit_picture_scraper',
                       username=username)


def try_direct_link(submission):
    if regex.search(
            "(\w)+" + REGEX_IMAGE_EXT, submission.url):
        file_name = regex.search(
            "(\w)+" + REGEX_IMAGE_EXT, submission.url)
        if not os.path.isfile(DIR_PATH + file_name.group(0)):
            data = requests.get(submission.url).content
            with open(DIR_PATH + file_name.group(0), "wb") as image_file:
                image_file.write(data)
        return True
    return False


def try_imgur_album(submission):
    if regex.search("http(s?)://imgur.com/a/(.*)", submission.url):
        image_link = ""
        album_data = requests.get("https://api.imgur.com/3/album/" + regex.search("http(s?)://imgur.com/a/(.*)", submission.url).group(
            2), headers={'Authorization': "Client-ID 1f42573b33c7586"}).json()["data"]

        for image_index in range(album_data["images_count"]):
            image_link = requests.get("https://api.imgur.com/3/album/" + regex.search("http(s?)://imgur.com/a/(.*)", submission.url).group(
                2), headers={'Authorization': "Client-ID 1f42573b33c7586"}).json()["data"]["images"][image_index]["link"]

            file_name = regex.search(
                "(\w)+" + REGEX_IMAGE_EXT, image_link)

            if not os.path.isfile(DIR_PATH + file_name.group(0)):
                data = requests.get(image_link).content
                with open(DIR_PATH + file_name.group(0), "wb") as image_file:
                    image_file.write(data)


def try_imgur_image(submission):
    if not regex.search("http(s?)://imgur.com/a/(.*)", submission.url):
        image_link = ""
        image_link = requests.get("https://api.imgur.com/3/image/" + regex.search("http(s?)://imgur.com/(.*)", submission.url).group(
            2), headers={'Authorization': "Client-ID 1f42573b33c7586"}).json()["data"]["link"]

        file_name = regex.search(
            "(\w)+" + REGEX_IMAGE_EXT, image_link)

        if not os.path.isfile(DIR_PATH + file_name.group(0)):
            data = requests.get(image_link).content
            with open(DIR_PATH + file_name.group(0), "wb") as image_file:
                image_file.write(data)


def try_imgur_link(submission):
    if regex.search("http(s?)://imgur.com/(.*)", submission.url):
        # Imgur has a few different link formats
        try_imgur_album(submission)
        try_imgur_image(submission)
        return True
    return False


def process(reddit):
    # Check for download dir to be present
    if not os.path.exists(DIR_PATH):
        os.makedirs(DIR_PATH)

    # process all saved submissions
    LIMIT = 100
    done = False
    count = 0
    params = {}
    processed = []
    unprocessed = []
    while not done:
        for submission in reddit.user.me().saved(limit=LIMIT, params=params):
            count += 1
            last_submission = submission
            try:
                if try_direct_link(submission) or try_imgur_link(submission):
                    print("processed " + submission.id)
                    processed.append(submission)
                else:
                    # could not process
                    unprocessed.append(submission)

            except Exception:
                # could not process
                unprocessed.append(submission)

        params["after"] = "t3_" + last_submission.id
        done = count < LIMIT
        count = 0

    return [processed, unprocessed]


def main():
    try:
        [processed, unprocessed] = process(login())

        for submission in processed:
            submission.unsave()
        print("\nScript finished!")
        if len(unprocessed) > 0:
            print("Was unable to process:")
            for code in unprocessed:
                print("www.reddit.com/" + code.id)

    except Exception as e:
        print("\nError! Check username, password, and internet connection.")
        more_info = input("Want more info? [y/N] ")
        if more_info in ["y", "Y", "yes", "Yes"]:
            print(e)


main()
